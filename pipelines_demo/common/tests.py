from django.test import TestCase


class HomepageTestCase(TestCase):
    def test_homepage_welcome(self):
        """
        Visiting the application homepage should display nice welcome message
        """
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Welcome to Pipelines Demo!")
