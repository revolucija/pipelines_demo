from django.views.generic import TemplateView


class HomepageView(TemplateView):
    template_name = 'common/index.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HomepageView, self).get_context_data(*args, **kwargs)
        # Add custom context for homepage here
        return context
