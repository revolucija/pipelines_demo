from base import *


ALLOWED_HOSTS = [
    # Add your staging domain here
]

WSGI_APPLICATION = 'pipelines_demo.wsgi_staging.application'
