from base import *


ALLOWED_HOSTS = [
    # Add your production domain here
]

WSGI_APPLICATION = 'pipelines_demo.wsgi_production.application'
