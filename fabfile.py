import os

from fabric.api import task, env, run, cd
from fabric.context_managers import prefix

# initialize project settings
settings = {}


def set_env(kind='staging'):
    """Setup environment variables."""
    global settings
    try:
        # first, read some explicitly defined settings from env file
        with open(os.path.join(os.environ.get('PWD'), kind), 'r') as f:
            for line in f:
                if line.startswith('export'):
                    s = line.strip().split(' ')[1].split('=')
                    # we can read only explicitly defined settings,
                    # we cannot parse bash variables here
                    if s[0] in ['PROJECT', 'HOST', 'PROJECT_FOLDER']:
                        settings[s[0]] = s[1]
        # project folder = project or project_staging
        if kind == 'production':
            settings['PROJECT_FOLDER'] = settings['PROJECT']
        else:
            settings['PROJECT_FOLDER'] = settings['PROJECT'] + '_staging'
        # second, build rest of the settings based on settings
        # we've just read from env file
        settings['BASE_DIR'] = os.path.join('/home/revolucija/webapps',
                                            settings['PROJECT_FOLDER'])
        settings['PROJECT_REPO'] = os.path.join(settings['BASE_DIR'], 'code')
        settings['PROJECT_WSGI'] = os.path.join(settings['PROJECT_REPO'],
                                                settings['PROJECT'],
                                                settings['PROJECT'])
        settings['MANAGEPY'] = os.path.join(settings['PROJECT_REPO'],
                                            settings['PROJECT'],
                                            'manage.py')
        settings['PY'] = os.path.join(settings['BASE_DIR'],
                                      'env/bin/python2.7')
        # third, set fab env settings
        env.host_string = settings['HOST']
        env.forward_agent = True
    except IOError:
        print "Cannot find env file {}".format(kind)
    except Exception as e:
        print "fabfile set_env exception {}: {}".format(type(e), e)


@task
def deploy(kind='staging'):
    set_env(kind)
    with cd(settings['PROJECT_REPO']):
        # pull code
        if kind == 'production':
            run('git pull --rebase origin master')
        elif kind == 'staging':
            run('git pull --rebase origin development')
        # install requirements, run migrations and collect static
        with prefix('source {}'.format(kind)):
            run('pip install -r requirements/{}.txt'.format(kind))
            run('{} {} migrate'.format(settings['PY'], settings['MANAGEPY']))
            run('{} {} collectstatic --noinput'.
                format(settings['PY'], settings['MANAGEPY']))
    # restart server
    with cd(settings['PROJECT_WSGI']):
            run('touch wsgi_{}.py'.format(kind))
