# Pipelines Demo

This repository contains demo Django project for testing [BitBucket's Pipelines](https://bitbucket.org/product/features/pipelines).


# Get It

Fork this repository to your own account, then clone it:

    git clone git@bitbucket.org:<your_account_name>/pipelines_demo.git

# Run It Locally

Create postgres database and database user:

    createuser -D -P pipelines_demo
    createdb -O pipelines_demo pipelines_demo

Create virtualenv and install dependencies:

    mkvirtualenv pipelines_demo
    . development
    pip install -r requirements/dev.txt

Run migrations and start server:

    django-admin migrate
    django-admin runserver

# Continuous Delivery With Bitbucket Pipelines

We have wrote a detailed [blog post](TODO Add link) explaining how to test and deploy Django app with Bitbucket Pipelines.

In short, here are the steps you need to do:

1. [Enable Pipelines](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html) in your Bitbucket repo settings

2. [Configure Pipelines environment variables](https://confluence.atlassian.com/bitbucket/environment-variables-in-bitbucket-pipelines-794502608.html). Add following environment variables:

        BASE_DIR=/opt/atlassian/pipelines/agent/build
        DATABASE_URL=postgres://pipelines_demo:so_strong@localhost:5432/pipelines_demo
        DJANGO_SETTINGS_MODULE=pipelines_demo.settings.test
        POSTGRESQL_DATABASE=pipelines_demo
        POSTGRESQL_PASSWORD=so_strong
        POSTGRESQL_USER=pipelines_demo
        PYTHONPATH=/opt/atlassian/pipelines/agent/build/pipelines_demo:/opt/atlassian/pipelines/agent/build/pipelines_demo/pipelines_demo:/opt/atlassian/pipelines/agent/build/pipelines_demo/pipelines_demo/pipelines_demo
        SECRET_KEY=super_secret_key
        DEPLOYMENT_SSH_KEY=****

    [Encode the private key](https://confluence.atlassian.com/bitbucket/access-remote-hosts-via-ssh-847452940.html) and add it as secure environment variable.

3. [Install the public key](https://confluence.atlassian.com/bitbucket/access-remote-hosts-via-ssh-847452940.html) on remote host and add it as Deployment key to your Bitbucket repo

4. [Add public key](https://confluence.atlassian.com/bitbucket/access-remote-hosts-via-ssh-847452940.html) of remote host to project's known_hosts file

5. Set HOST variable to your host in production and staging files

6. commit changes and push them to your git repository

7. Open your Bitbucket repository in browser and check Pipelines progress
